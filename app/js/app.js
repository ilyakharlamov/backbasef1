(function iife_app () {
  'use strict';
  angular
  .module('backbaseF1', [
    'backbaseF1.Controllers',
    'backbaseF1.services',
    'ngRoute'
  ]).
  config(['$routeProvider', route ]);

  function route ($routeProvider) {
    $routeProvider.
      when("/seasons", {templateUrl: "html/seasons.html", controller: "seasonsController"}).
      when("/season/:id", {templateUrl: "html/season.html", controller: "seasonController"}).
      otherwise({redirectTo: '/seasons'});
  }
})();


