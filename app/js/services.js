(function iife_services () {
  'use strict';
  // constants are in capitalcase
  var BASE = 'http://ergast.com/api/f1/';
  var SUFFIX = '.json?callback=JSON_CALLBACK';

  angular.
  module('backbaseF1.services', []).
  factory('f1apiservice', f1apiservice).
  factory('seasonservice', seasonservice);

  /**
  * technically the list of required seasons
  * is not a part of ergast api
  */
  function seasonservice ($q) {
    return {getSeasons : getSeasons};

    /**
    * There is no real need for this function be async
    * but for consistency the async is better here
    */
    function getSeasons (startYear, endYear) {
      return $q(function requiredSeasonsPromise(resolve, reject) {
        setTimeout(function() {
          resolve(generateSeasons(startYear, endYear));
        }, 15);
      });
    }

    function generateSeasons (startYear, endYear) {
      endYear = endYear || new Date().getFullYear();
      startYear = startYear || 2005;
      var seasons = [];
      var currYear;
      for (currYear=startYear; currYear <= endYear; currYear++) {
        seasons.push({
          name: ''+currYear,
          id: currYear
        });
      }
      return seasons;
    }
  }
  /**
  * always use named functions instead of anonymous
  * in order to trace it better in stacktrace
  */
  function f1apiservice ($http) {
    return {
      getRaces : getRaces,
      getDriverStandings : getDriverStandings
    };

    function getDriverStandings (seasonId) {
      return callRest($http, [seasonId, 'driverStandings', 1]);
    }

    function getRaces (seasonId) {
      return callRest($http, [seasonId, 'results', 1]);
    }
  }
  /* converts any object to string */
  function lambdaStr (i) {
    return ''+i;
  }
  /**
  * formats the rest api url
  * example: restApiUrl({mykey: myvalue}) => .../mykey/myvalue/...
  */
  function restUrl ( params ) {
    params = params || [];
    return BASE + params.map(lambdaStr).join('/') + SUFFIX;
  }
  /**
  * functions should be as small as possible
  * so you could combine them with others
  * and therefore reuse heavily
  * functions more than 10 lines length are not readable
  * also smaller functions are easier to test
  * thats why I split the callRest into callRest and restUrl
  */
  function callRest ($http, params) {
    return $http({
      method: 'JSONP',
      url: restUrl(params)
    });
  }

})();

