(function iife () {
  'use strict';

  var DEFAULT_START_YEAR = 2005;
  var DEFAULT_END_YEAR = 2015;

  angular.
    module('backbaseF1.Controllers', []).
    controller('seasonsController', seasonsController).
    controller('seasonController', seasonController);

  function seasonsController ($scope, seasonservice) {
    seasonservice.
    getSeasons(DEFAULT_START_YEAR, DEFAULT_END_YEAR).
    then(function (seasons) {
      $scope.seasons = seasons;
    });
  }

  function seasonController ($scope, $routeParams, f1apiservice) {
    var seasonId = $routeParams.id;
    $scope.isRacesLoaded = false;
    f1apiservice.
      getRaces(seasonId).
      success(function (response) {
        var raceTable = response.MRData.RaceTable;
        $scope.races = raceTable.Races.map(formatRace);
        $scope.isRacesLoaded = true;
      });
    /**
    * even though those requests are async
    * there is no need to chain sequential ajax here
    * if season comes before races, races are not drawn
    * if races come before season, races are drawn but no 
    * winner higlighted which is fine for progressive load
    * when all the data comes
    * angular will detect the dirty scope and redraw it all
    */
    $scope.isSeasonLoaded = false;
    f1apiservice.
      getDriverStandings(seasonId).
      success(function (response) {
        var table = response.MRData.StandingsTable;
        var driver = table.StandingsLists[0].DriverStandings[0].Driver;
        $scope.season = { name: table.season, winner: formatDriver(driver)};
        $scope.isSeasonLoaded = true;
      });
  }

    /*
  * convert response race to model race
  */
  function formatRace (race) {
    var driver = race.Results[0].Driver;
    return {
      name: race.raceName,
      winner: formatDriver(driver)
    };
  }
  /**
  * aggregate driver data from response
  */
  function formatDriver (driver) {
    return {
      id: driver.driverId,
      name: driver.familyName + ' ' + driver.givenName
    };
  }

})();


